package com.bestkilled.softdev02;

import java.util.Scanner;

public class OX {

    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);
        String[][] games = new String[3][4];
        System.out.println("Welcome to OX Game");
        String[] x = {"1", "2", "3"};
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                games[i][j] = "-";
            }
        }

        for (int i = 0; i < 3; i++) {
            System.out.print(" " + x[i]);
        }
        System.out.println();
        for (int i = 0; i < 3; i++) {
            System.out.print(x[i]);
            for (int j = 0; j < 3; j++) {
                System.out.print(games[i][j] + " ");
            }
            System.out.println();
        }
        String turn = "X";
        System.out.println(turn + " turn");
        System.out.println("Please input Row Col : ");
        int ip1 = kb.nextInt();
        int ip2 = kb.nextInt();

        while ((ip1 < 1 || ip1 > 3 || ip2 < 1 || ip2 > 3)) {
            System.out.println("Not found, pls try again by select other position");
            System.out.println("Please input Row Col : ");
            ip1 = kb.nextInt();
            ip2 = kb.nextInt();

        }
        if (games[ip1 - 1][ip2 - 1].equals("-")) {
            games[ip1 - 1][ip2 - 1] = turn;
        } else {
            while (!games[ip1 - 1][ip2 - 1].equals("-")) {
                System.out.println("Pls try again by select other position");
                System.out.println("Please input Row Col : ");
                ip1 = kb.nextInt();
                ip2 = kb.nextInt();
                while ((ip1 < 1 || ip1 > 3 || ip2 < 1 || ip2 > 3)) {
                    System.out.println("Not found, pls try again by select other position");
                    System.out.println("Please input Row Col : ");
                    ip1 = kb.nextInt();
                    ip2 = kb.nextInt();

                }
            }games[ip1 - 1][ip2 - 1] = turn;
            
        }

        int winx = 0, winy = 0, di = 0, di2 = 0;
        int winx1 = 0, winy1 = 0, di1 = 0, di21 = 0;
        int round = 0;
        boolean check = true;
        while (check == true) {
            round++;
            for (int i = 0; i < 3; i++) {
                for (int j = 0, ti = 2; j < 3; j++, ti--) {

                    if (games[i][j].equals("X")) {
                        winx++;
                    }
                    if (games[j][i].equals("X")) {
                        winy++;
                    }
                    if (games[j][j].equals("X")) {
                        di++;
                    }

                    if (games[j][ti].equals("X")) {
                        di2++;
                    }

                    if (games[i][j].equals("O")) {
                        winx1++;
                    }
                    if (games[j][i].equals("O")) {
                        winy1++;
                    }
                    if (games[j][j].equals("O")) {
                        di1++;
                    }
                    if (games[j][ti].equals("O")) {
                        di21++;
                    }
                }

                if (winx == 3 || winy == 3 || di == 3 || di2 == 3) {
                    for (int ik = 0; ik < 3; ik++) {
                        System.out.print(" " + x[ik]);
                    }
                    System.out.println();
                    for (int ik = 0; ik < 3; ik++) {
                        System.out.print(x[ik]);
                        for (int jk = 0; jk < 3; jk++) {
                            System.out.print(games[ik][jk] + " ");
                        }
                        System.out.println();
                    }
                    System.out.println("Player X win .....");
                    check = false;
                    break;
                } else if (winx1 == 3 || winy1 == 3 || di1 == 3 || di21 == 3) {
                    for (int ik = 0; ik < 3; ik++) {
                        System.out.print(" " + x[ik]);
                    }
                    System.out.println();
                    for (int ik = 0; ik < 3; ik++) {
                        System.out.print(x[ik]);
                        for (int jk = 0; jk < 3; jk++) {
                            System.out.print(games[ik][jk] + " ");
                        }
                        System.out.println();
                    }
                    System.out.println("Player O win .....");
                    check = false;
                    break;
                } else {
                    winx1 = 0;
                    winx = 0;
                    winy1 = 0;
                    winy = 0;
                    di1 = 0;
                    di = 0;
                    di21 = 0;
                    di2 = 0;
                }
            }
            if (round == 9) {
                for (int ik = 0; ik < 3; ik++) {
                    System.out.print(" " + x[ik]);
                }
                System.out.println();
                for (int ik = 0; ik < 3; ik++) {
                    System.out.print(x[ik]);
                    for (int jk = 0; jk < 3; jk++) {
                        System.out.print(games[ik][jk] + " ");
                    }
                    System.out.println();
                }
                System.out.println("Draw .....");
                check = false;
            }
            if (check == false) {
                System.out.println("Bye bye . . . .");
                break;
            }
            if (turn.equals("X")) {
                turn = "O";
            } else if (turn.equals("O")) {
                turn = "X";
            }
            for (int i = 0; i < 3; i++) {
                System.out.print(" " + x[i]);
            }
            System.out.println();
            for (int i = 0; i < 3; i++) {
                System.out.print(x[i]);
                for (int j = 0; j < 3; j++) {
                    System.out.print(games[i][j] + " ");
                }
                System.out.println();
            }
            System.out.println(turn + " turn");
            System.out.println("Please input Row Col : ");
            ip1 = kb.nextInt();
            ip2 = kb.nextInt();
            while ((ip1 < 1 || ip1 > 3 || ip2 < 1 || ip2 > 3)) {
            System.out.println("Not found, pls try again by select other position");
            System.out.println("Please input Row Col : ");
            ip1 = kb.nextInt();
            ip2 = kb.nextInt();

        }
        if (games[ip1 - 1][ip2 - 1].equals("-")) {
            games[ip1 - 1][ip2 - 1] = turn;
        } else {
            while (!games[ip1 - 1][ip2 - 1].equals("-")) {
                System.out.println("Pls try again by select other position");
                System.out.println("Please input Row Col : ");
                ip1 = kb.nextInt();
                ip2 = kb.nextInt();
                while ((ip1 < 1 || ip1 > 3 || ip2 < 1 || ip2 > 3)) {
                    System.out.println("Not found, pls try again by select other position");
                    System.out.println("Please input Row Col : ");
                    ip1 = kb.nextInt();
                    ip2 = kb.nextInt();

                }
            }games[ip1 - 1][ip2 - 1] = turn;
            
        }

        }

    }
}
